package com.technologies.capslassestment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.technologies.capslassestment.core.base.BaseActivity
import com.technologies.capslassestment.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override val layoutRes = R.layout.activity_main

    override fun onCreated(instance: Bundle?) {
        showLoading(true)

        Handler(Looper.getMainLooper()).postDelayed({
            runOnUiThread {
                showLoading(false)
            }
        }, 5000)
    }
}