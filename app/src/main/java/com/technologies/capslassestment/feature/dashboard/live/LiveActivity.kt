package com.technologies.capslassestment.feature.dashboard.live

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.SurfaceView
import com.technologies.capslassestment.R
import com.technologies.capslassestment.core.base.BaseActivity
import com.technologies.capslassestment.core.base.BaseRtcActivity
import com.technologies.capslassestment.core.extension.observe
import com.technologies.capslassestment.core.extension.viewModel
import com.technologies.capslassestment.core.utils.rtc.EngineConfig
import com.technologies.capslassestment.core.utils.rtc.StatsManager
import com.technologies.capslassestment.databinding.ActivityLiveBinding
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import kotlinx.android.synthetic.main.activity_live.*
import javax.inject.Inject

class LiveActivity : BaseRtcActivity<ActivityLiveBinding>(), LiveHandler {
    override val layoutRes = R.layout.activity_live

    @Inject
    lateinit var viewModel: LiveViewModel

    override fun addUserVideo(uid: Int, surfaceView: SurfaceView) {
        live_video_grid_layout.addUserVideoSurface(uid, surfaceView, false)
    }

    override fun removeUserVideo(uid: Int) {
        live_video_grid_layout.removeUserVideo(uid, false)
    }

    override fun onSwitchCamera() {
        rtcEngine.switchCamera()
    }

    override fun onLeaveChannel() {

        finish()
    }

    override fun onCreated(instance: Bundle?) {
        initObserver()
        initViews()
        checkExtras()
    }

    private fun initViews() {
        live_video_grid_layout.setStatsManager(statsManager)
    }

    private fun initObserver() {
        viewModel = viewModel(viewModelFactory) {
            observe(userRole) {
                it?.apply {
                    initChannel(this)
                }
            }
            observe(broadcaster) { it?.apply { if (this) startBroadcast() } }
        }
        binding.lifecycleOwner = this
        binding.vm = viewModel
        binding.handler = this
    }

    private fun startBroadcast() {
        val surfaceView = prepareRtcVideo(
            engineConfig.account?.uid ?: 0,
            true
        )
        live_video_grid_layout.addUserVideoSurface(
            engineConfig.account?.uid ?: 0,
            surfaceView,
            true
        )
    }

    private fun stopBroadcast() {
        viewModel.setUserRole(Constants.CLIENT_ROLE_AUDIENCE)
        removeRtcVideo(engineConfig.account?.uid ?: 0, true)
        live_video_grid_layout.removeUserVideo(engineConfig.account?.uid ?: 0, true)
    }

    private fun checkExtras() {
        intent.extras?.let { bundle ->

            if (bundle.containsKey(EXTRA_CHANNEL)) {
                setChannelName(bundle.getString(EXTRA_CHANNEL) ?: "")
            }

            if (bundle.containsKey(EXTRA_ROLE)) {
                viewModel.setUserRole(bundle.getInt(EXTRA_ROLE, Constants.CLIENT_ROLE_AUDIENCE))
            }
        }
    }

    companion object {
        const val EXTRA_ROLE = "_e_role"
        const val EXTRA_CHANNEL = "_e_channel"
    }
}