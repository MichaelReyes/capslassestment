package com.technologies.capslassestment.feature.dashboard.channel

import android.os.Bundle
import androidx.core.os.bundleOf
import com.technologies.capslassestment.R
import com.technologies.capslassestment.core.base.BaseActivity
import com.technologies.capslassestment.core.base.BaseFragment
import com.technologies.capslassestment.core.base.BaseViewModel
import com.technologies.capslassestment.core.extension.goToActivity
import com.technologies.capslassestment.core.extension.observe
import com.technologies.capslassestment.core.extension.viewModel
import com.technologies.capslassestment.core.network.api.channel.ChannelsPagedListAdapter
import com.technologies.capslassestment.databinding.FragmentChannelsBinding
import com.technologies.capslassestment.feature.dashboard.live.LiveActivity
import io.agora.rtc.Constants
import kotlinx.android.synthetic.main.fragment_channels.*
import javax.inject.Inject

class ChannelsFragment : BaseFragment<FragmentChannelsBinding>(), ChannelsHandler {

    @Inject
    lateinit var viewModel: ChannelsViewModel

    override val layoutRes = R.layout.fragment_channels

    lateinit var adapter: ChannelsPagedListAdapter

    override fun getViewModel(): BaseViewModel? {
        return viewModel
    }

    override fun onCreated(savedInstance: Bundle?) {

        (activity as? BaseActivity<*>)?.setToolbar(show = true, title = getString(R.string.app_name))

        initViews()
        initObserver()
    }

    override fun onNewChannel() {
        activity?.goToActivity(
            LiveActivity::class.java,
            false,
            bundleOf(
                Pair(LiveActivity.EXTRA_ROLE, Constants.CLIENT_ROLE_BROADCASTER),
            )
        )
    }

    private fun initViews() {
        adapter = ChannelsPagedListAdapter(ChannelsPagedListAdapter.ChannelsDiffUtilItemCallback())
        channels_rvData.adapter = adapter

        channels_swipeLayout.setOnRefreshListener {
            channels_swipeLayout.isRefreshing = false
            viewModel.sourceFactory.source.value?.invalidate()
        }

        adapter.clickListener = {
            activity?.goToActivity(
                LiveActivity::class.java,
                false,
                bundleOf(
                    Pair(LiveActivity.EXTRA_ROLE, Constants.CLIENT_ROLE_AUDIENCE),
                    Pair(LiveActivity.EXTRA_CHANNEL, it.channel_name)
                )
            )
        }
    }

    private fun initObserver() {
        viewModel = viewModel(viewModelFactory) {
            fetchChannels()
            observe(channels) {
                adapter.submitList(it)
            }
        }
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.handler = this
    }
}