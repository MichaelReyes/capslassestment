package com.technologies.capslassestment.feature.dashboard.channel

import com.technologies.capslassestment.core.data.pojo.channel.Channel
import com.technologies.capslassestment.core.exception.Failure
import com.technologies.capslassestment.core.functional.Either
import com.technologies.capslassestment.core.interactor.UseCase
import com.technologies.capslassestment.core.network.api.channel.ChannelsRepository
import javax.inject.Inject

class GetChannels
@Inject constructor(private val channelsRepository: ChannelsRepository) : UseCase<List<Channel>, GetChannels.Params>() {

    override suspend fun run(params: Params): Either<Failure, List<Channel>> = channelsRepository.channels(params.pageNumber, params.pageSize)

    data class Params(val pageNumber: Int, val pageSize: Int)
}
