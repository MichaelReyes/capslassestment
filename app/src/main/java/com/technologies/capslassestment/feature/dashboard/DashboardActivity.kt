package com.technologies.capslassestment.feature.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.technologies.capslassestment.R
import com.technologies.capslassestment.core.base.BaseActivity
import com.technologies.capslassestment.databinding.ActivityDashboardBinding
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity<ActivityDashboardBinding>() {
    override val layoutRes = R.layout.activity_dashboard

    override fun onCreated(instance: Bundle?) {
        setSupportActionBar(toolbar)
    }
}