package com.technologies.capslassestment.feature.dashboard.live

interface LiveHandler {

    fun onSwitchCamera()

    fun onLeaveChannel()

}