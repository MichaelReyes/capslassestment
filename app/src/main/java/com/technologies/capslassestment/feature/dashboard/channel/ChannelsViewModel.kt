package com.technologies.capslassestment.feature.dashboard.channel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.technologies.capslassestment.core.base.BaseViewModel
import com.technologies.capslassestment.core.data.datasource.channels.ChannelsDataSourceFactory
import com.technologies.capslassestment.core.data.pojo.channel.Channel
import javax.inject.Inject

class ChannelsViewModel
@Inject constructor(
    private val getChannels: GetChannels
) : BaseViewModel() {

    lateinit var sourceFactory: ChannelsDataSourceFactory

    private val pagedListLoading = MutableLiveData(false)
    private val pagedListLoadingObserver = Observer<Boolean> {
        setLoading(it)
    }

    val pagedListEmpty = MutableLiveData(false)
    var channels: LiveData<PagedList<Channel>> = MutableLiveData<PagedList<Channel>>()

    init {
        pagedListLoading.observeForever(pagedListLoadingObserver)
    }

    fun fetchChannels(){
        val config = PagedList.Config.Builder()
            .setPageSize(10)
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(10)
            .build()

        sourceFactory = ChannelsDataSourceFactory(getChannels, pagedListLoading, pagedListEmpty)

        channels = LivePagedListBuilder(sourceFactory, config).build()
    }
}
