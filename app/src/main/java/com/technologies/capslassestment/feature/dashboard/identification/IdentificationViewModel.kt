package com.technologies.capslassestment.feature.dashboard.identification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.technologies.capslassestment.BuildConfig
import com.technologies.capslassestment.core.base.BaseViewModel
import com.technologies.capslassestment.core.data.pojo.auth.Account
import com.technologies.capslassestment.core.utils.rtc.EngineConfig
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@ExperimentalCoroutinesApi
@FlowPreview
class IdentificationViewModel
@Inject constructor(
    private val rtcEngine: RtcEngine,
    private val engineConfig: EngineConfig
) : BaseViewModel() {

    private val _name = MutableLiveData("")
    val name: LiveData<String> = _name

    private val _nameValid = MutableLiveData(false)
    val nameValid: LiveData<Boolean> = _nameValid

    private val _account = MutableLiveData<Account>()
    val account: LiveData<Account> = _account

    private val nameStream = BroadcastChannel<String>(Channel.CONFLATED)

    private suspend fun observeNameStream() {
        nameStream.asFlow().debounce(100)
            .collect {
                _name.value = it
                _nameValid.value = it.isNotEmpty()
            }
    }

    init {
        viewModelScope.launch {
            observeNameStream()
        }

        rtcEngine.addHandler(object : IRtcEngineEventHandler() {
            override fun onLocalUserRegistered(uid: Int, userAccount: String?) {
                super.onLocalUserRegistered(uid, userAccount)
                userAccount?.apply {
                    val account = Account(uid, this)
                    _account.postValue(account)
                    engineConfig.account = account
                }
            }
        })
    }

    fun onNameChange(text: CharSequence) {
        viewModelScope.launch {
            nameStream.send(text.toString())
        }
    }

    fun registerAccount() {
        _name.value?.let { name ->
            rtcEngine.registerLocalUserAccount(
                BuildConfig.APP_ID,
                "$name${Date().time}"
            )
        }
    }
}
