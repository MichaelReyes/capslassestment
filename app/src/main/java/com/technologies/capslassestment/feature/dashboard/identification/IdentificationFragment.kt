package com.technologies.capslassestment.feature.dashboard.identification

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.technologies.capslassestment.R
import com.technologies.capslassestment.core.base.BaseActivity
import com.technologies.capslassestment.core.base.BaseFragment
import com.technologies.capslassestment.core.base.BaseViewModel
import com.technologies.capslassestment.core.extension.observe
import com.technologies.capslassestment.core.extension.viewModel
import com.technologies.capslassestment.databinding.FragmentIdentificationBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

@ExperimentalCoroutinesApi
@FlowPreview
class IdentificationFragment : BaseFragment<FragmentIdentificationBinding>(),
    IdentificationHandler {

    override val layoutRes = R.layout.fragment_identification

    @Inject
    lateinit var viewModel: IdentificationViewModel


    override fun onCreated(savedInstance: Bundle?) {

        (activity as? BaseActivity<*>)?.setToolbar(show = false)

        initObserver()
    }

    private fun initObserver() {
        viewModel = viewModel(viewModelFactory) {
            observe(account){
                it?.apply { findNavController().navigate(R.id.action_goTo_channels) }
            }
        }
        binding.lifecycleOwner = this
        binding.vm = viewModel
        binding.handler = this
    }

    @ExperimentalCoroutinesApi
    override fun getViewModel(): BaseViewModel? = viewModel

    override fun onContinue() {
        viewModel.registerAccount()
    }
}