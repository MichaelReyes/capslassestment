package com.technologies.capslassestment.feature.splash

import android.Manifest
import android.os.Bundle
import com.technologies.capslassestment.MainActivity
import com.technologies.capslassestment.R
import com.technologies.capslassestment.core.base.BaseActivity
import com.technologies.capslassestment.core.extension.checkPermissions
import com.technologies.capslassestment.core.extension.goToActivity
import com.technologies.capslassestment.databinding.ActivitySplashBinding
import com.technologies.capslassestment.feature.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    override val layoutRes = R.layout.activity_splash

    override fun onCreated(instance: Bundle?) {
        splash_animationView.addAnimatorUpdateListener {
            if ((it.animatedValue as Float * 100).toInt() == 99)
                GlobalScope.launch { proceed() }
        }
    }

    private suspend fun proceed() {
        delay(1000)
        checkPermissions()
    }

    private fun checkPermissions() {
        checkPermissions(
            listOf(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.MODIFY_AUDIO_SETTINGS,
                Manifest.permission.BLUETOOTH
            )
        ) { accepted ->
            if (!accepted) {
                checkPermissions()
            } else {
                goToActivity(DashboardActivity::class.java, true)
            }
        }
    }

}