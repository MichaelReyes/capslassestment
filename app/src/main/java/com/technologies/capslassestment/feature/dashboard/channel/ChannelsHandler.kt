package com.technologies.capslassestment.feature.dashboard.channel

interface ChannelsHandler {

    fun onNewChannel()

}