package com.technologies.capslassestment.feature.dashboard.live

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.huawei.multimedia.audiokit.utils.Constant
import com.technologies.capslassestment.core.base.BaseViewModel
import com.technologies.capslassestment.core.data.datasource.channels.ChannelsDataSourceFactory
import com.technologies.capslassestment.core.data.pojo.channel.Channel
import io.agora.rtc.Constants
import javax.inject.Inject

class LiveViewModel
@Inject constructor() : BaseViewModel() {

    private val _userRole = MutableLiveData(Constants.CLIENT_ROLE_AUDIENCE)
    val userRole: LiveData<Int> = _userRole
    fun setUserRole(role: Int){
        _userRole.value = role
        _broadcaster.value = (role == Constants.CLIENT_ROLE_BROADCASTER)
    }

    private val _broadcaster = MutableLiveData(false)
    val broadcaster: LiveData<Boolean> = _broadcaster

}
