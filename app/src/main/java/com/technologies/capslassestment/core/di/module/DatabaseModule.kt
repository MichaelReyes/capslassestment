package com.technologies.capslassestment.core.di.module

import android.content.Context
import androidx.room.Room
import com.technologies.capslassestment.core.data.db.AppDatabase
import com.technologies.capslassestment.core.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    @AppScope
    @Provides
    fun provideRoomDatabase(context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }


}