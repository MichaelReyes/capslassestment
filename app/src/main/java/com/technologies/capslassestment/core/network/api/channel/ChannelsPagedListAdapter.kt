package com.technologies.capslassestment.core.network.api.channel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.technologies.capslassestment.R
import com.technologies.capslassestment.core.data.pojo.channel.Channel
import com.technologies.capslassestment.databinding.ItemChannelBinding


class ChannelsPagedListAdapter constructor(
    @NonNull val diffCallback: DiffUtil.ItemCallback<Channel>
) : PagedListAdapter<Channel, RecyclerView.ViewHolder>(diffCallback) {


    internal var clickListener: (Channel) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemChannelBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_channel, parent, false
        )
        return Holder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as Holder
        viewHolder.bind(getItem(position)!!)
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding: ItemChannelBinding? = DataBindingUtil.bind(itemView)

        internal fun bind(channel: Channel) {
            binding?.apply {
                item = channel
                itemView.setOnClickListener { clickListener(channel) }
                executePendingBindings()
            }
        }
    }

    class ChannelsDiffUtilItemCallback : DiffUtil.ItemCallback<Channel>() {
        override fun areItemsTheSame(oldItem: Channel, newItem: Channel): Boolean {
            return oldItem.channel_name == newItem.channel_name
        }

        override fun areContentsTheSame(oldItem: Channel, newItem: Channel): Boolean {
            return oldItem.channel_name == newItem.channel_name
        }
    }
}

