package com.technologies.capslassestment.core.data.pojo.channel

data class Channels(
    val channels: List<Channel>
)