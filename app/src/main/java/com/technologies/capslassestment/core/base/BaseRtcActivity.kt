package com.technologies.capslassestment.core.base

import android.os.Bundle
import android.view.SurfaceView
import androidx.databinding.ViewDataBinding
import com.technologies.capslassestment.BuildConfig
import com.technologies.capslassestment.core.utils.Constants
import com.technologies.capslassestment.core.utils.rtc.AgoraEventHandler
import com.technologies.capslassestment.core.utils.rtc.EngineConfig
import com.technologies.capslassestment.core.utils.rtc.EventHandler
import com.technologies.capslassestment.core.utils.rtc.StatsManager
import io.agora.rtc.IRtcChannelEventHandler
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcChannel
import io.agora.rtc.RtcEngine
import io.agora.rtc.models.ChannelMediaOptions
import io.agora.rtc.video.VideoCanvas
import io.agora.rtc.video.VideoEncoderConfiguration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

abstract class BaseRtcActivity<V : ViewDataBinding> : BaseActivity<V>(), EventHandler {

    @Inject
    lateinit var rtcEngine: RtcEngine

    @Inject
    lateinit var engineConfig: EngineConfig

    @Inject
    lateinit var statsManager: StatsManager

    @Inject
    lateinit var eventHandler: AgoraEventHandler

    private var channelName = ""

    private var broadcastingChannel: RtcChannel? = null

    override fun onFirstRemoteVideoDecoded(uid: Int, width: Int, height: Int, elapsed: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            renderRemoteUser(uid)
        }

    }

    override fun onLeaveChannel(stats: IRtcEngineEventHandler.RtcStats?) {}

    override fun onJoinChannelSuccess(channel: String?, uid: Int, elapsed: Int) {}

    override fun onUserOffline(uid: Int, reason: Int) {
        removeRemoteUser(uid)
    }


    override fun onUserJoined(uid: Int, elapsed: Int) {}

    override fun onLastmileQuality(quality: Int) {}

    override fun onLastmileProbeResult(result: IRtcEngineEventHandler.LastmileProbeResult?) {}

    override fun onLocalVideoStats(stats: IRtcEngineEventHandler.LocalVideoStats?) {}

    override fun onRtcStats(stats: IRtcEngineEventHandler.RtcStats?) {}

    override fun onNetworkQuality(uid: Int, txQuality: Int, rxQuality: Int) {}

    override fun onRemoteVideoStats(stats: IRtcEngineEventHandler.RemoteVideoStats?) {}

    override fun onRemoteAudioStats(stats: IRtcEngineEventHandler.RemoteAudioStats?) {}

    override fun onConnectionStateChanged(state: Int, reason: Int) {}

    abstract fun addUserVideo(uid: Int, surfaceView: SurfaceView)

    abstract fun removeUserVideo(uid: Int)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configVideo()
        eventHandler.addHandler(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        broadcastingChannel?.apply {
            unpublish()
            destroy()
        }
        rtcEngine.leaveChannel()
        eventHandler.removeHandler(this)
    }

    private fun configVideo() {
        val configuration = VideoEncoderConfiguration(
            Constants.VIDEO_DIMENSIONS[2],
            VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
            VideoEncoderConfiguration.STANDARD_BITRATE,
            VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT
        )
        configuration.mirrorMode = Constants.VIDEO_MIRROR_MODES[0]
        rtcEngine.setVideoEncoderConfiguration(configuration)
        rtcEngine.setClientRole(io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE)
    }

    protected fun setChannelName(name: String) {
        channelName = name
    }

    protected fun initChannel(role: Int) {
        rtcEngine.setClientRole(role)
        if (role == io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER) {
            channelName = "CAPSL_ANDROID_${Date().time}"
            createChannel(channelName)
        } else {
            joinChannel(channelName)
        }
    }

    private fun createChannel(channelName: String) {
        val channel = rtcEngine.createRtcChannel(channelName)

        channel?.let {
            broadcastingChannel = it
            it.setRtcChannelEventHandler(object : IRtcChannelEventHandler() {
                override fun onJoinChannelSuccess(rtcChannel: RtcChannel?, uid: Int, elapsed: Int) {
                    super.onJoinChannelSuccess(rtcChannel, uid, elapsed)
                }
            })

            it.publish()

            joinChannel(channelName, it)
        }
    }

    private fun joinChannel(channelName: String, channel: RtcChannel? = null) {

        channel?.let { rtcChannel ->
            val mediaOptions = ChannelMediaOptions()
            mediaOptions.autoSubscribeAudio = true
            mediaOptions.autoSubscribeVideo = true

            rtcChannel.joinChannelWithUserAccount(
                BuildConfig.APP_ID,
                channelName,
                mediaOptions
            )

        }?:run {
            rtcEngine.joinChannelWithUserAccount(
                BuildConfig.APP_ID,
                channelName,
                engineConfig.account?.userAccount
            )
        }
    }

    protected fun prepareRtcVideo(uid: Int, local: Boolean): SurfaceView? {
        val surface = RtcEngine.CreateRendererView(this)
        if (local) {
            surface.setZOrderMediaOverlay(true)
            rtcEngine.setupLocalVideo(
                VideoCanvas(
                    surface,
                    VideoCanvas.RENDER_MODE_HIDDEN,
                    uid,
                    Constants.VIDEO_MIRROR_MODES[0]
                )
            )
        } else {
            rtcEngine.setupRemoteVideo(
                VideoCanvas(
                    surface,
                    VideoCanvas.RENDER_MODE_HIDDEN,
                    uid,
                    Constants.VIDEO_MIRROR_MODES[0]
                )
            )
        }
        return surface
    }

    protected fun removeRtcVideo(uid: Int, local: Boolean) {
        if (local) {
            rtcEngine.setupLocalVideo(null)
        } else {
            rtcEngine.setupRemoteVideo(VideoCanvas(null, VideoCanvas.RENDER_MODE_HIDDEN, uid))
        }
    }

    private fun removeRemoteUser(uid: Int) {
        removeRtcVideo(uid, false)
        removeUserVideo(uid)
    }

    private fun renderRemoteUser(uid: Int) {
        prepareRtcVideo(uid, false)?.let { surface ->
            addUserVideo(uid, surface)
        }
    }
}