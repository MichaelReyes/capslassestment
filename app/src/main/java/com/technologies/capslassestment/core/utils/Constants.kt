package com.technologies.capslassestment.core.utils

import io.agora.rtc.Constants
import io.agora.rtc.video.VideoEncoderConfiguration
import io.agora.rtc.video.VideoEncoderConfiguration.VideoDimensions

class Constants {
    companion object {
        const val PREF_KEY_NETWORK_CONNECTED = "_pref_network_connected"

        var VIDEO_DIMENSIONS = arrayOf(
            VideoEncoderConfiguration.VD_320x240,
            VideoEncoderConfiguration.VD_480x360,
            VideoEncoderConfiguration.VD_640x360,
            VideoEncoderConfiguration.VD_640x480,
            VideoDimensions(960, 540),
            VideoEncoderConfiguration.VD_1280x720
        )

        val VIDEO_MIRROR_MODES = intArrayOf(
            Constants.VIDEO_MIRROR_MODE_AUTO,
            Constants.VIDEO_MIRROR_MODE_ENABLED,
            Constants.VIDEO_MIRROR_MODE_DISABLED
        )
    }
}