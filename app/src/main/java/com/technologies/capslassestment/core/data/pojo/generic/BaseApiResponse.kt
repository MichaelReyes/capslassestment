package com.technologies.capslassestment.core.data.pojo.generic

import com.google.gson.annotations.SerializedName

data class BaseApiResponse<T>(
    @SerializedName("data")
    val result: T,
    val success: Boolean
)