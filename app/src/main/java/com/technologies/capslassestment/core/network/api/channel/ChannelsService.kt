package com.technologies.capslassestment.core.network.api.channel

import com.technologies.capslassestment.core.data.pojo.channel.Channels
import com.technologies.capslassestment.core.data.pojo.generic.BaseApiResponse
import com.technologies.capslassestment.core.di.scope.AppScope
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

@AppScope
class ChannelsService
@Inject constructor(retrofit: Retrofit) : ChannelsApi {
    private val api by lazy { retrofit.create(ChannelsApi::class.java) }

    override fun channels(pageNumber: Int, pageSize: Int) = api.channels(pageNumber, pageSize)
}
