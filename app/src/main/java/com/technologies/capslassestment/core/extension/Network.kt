package com.technologies.capslassestment.core.extension

import com.technologies.capslassestment.core.exception.Failure
import com.technologies.capslassestment.core.functional.Either
import retrofit2.Call

fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
    return try {
        val response = call.execute()
        when (response.isSuccessful) {
            true -> Either.Right(transform((response.body() ?: default)))
            false -> Either.Left(Failure.ApiError(response.errorBody()))
        }
    } catch (exception: Throwable) {
        Either.Left(Failure.ServerError)
    }
}