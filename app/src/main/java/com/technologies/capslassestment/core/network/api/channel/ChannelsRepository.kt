package com.technologies.capslassestment.core.network.api.channel

import com.technologies.capslassestment.core.data.pojo.channel.Channel
import com.technologies.capslassestment.core.data.pojo.channel.Channels
import com.technologies.capslassestment.core.exception.Failure
import com.technologies.capslassestment.core.extension.request
import com.technologies.capslassestment.core.functional.Either
import com.technologies.capslassestment.core.network.NetworkHandler
import javax.inject.Inject

interface ChannelsRepository {
    fun channels(pageNumber: Int, pageSize: Int): Either<Failure, List<Channel>>

    class Network
    @Inject constructor(
        private val networkHandler: NetworkHandler,
        private val service: ChannelsService
    ) : ChannelsRepository {

        override fun channels(pageNumber: Int, pageSize: Int): Either<Failure, List<Channel>> {
            return when (networkHandler.isConnected) {
                true -> request(
                    service.channels(pageNumber, pageSize),
                    { it?.result?.channels ?: emptyList() },
                    null
                )
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }
    }
}