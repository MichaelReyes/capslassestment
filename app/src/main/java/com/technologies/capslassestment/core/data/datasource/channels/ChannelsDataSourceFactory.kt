package com.technologies.capslassestment.core.data.datasource.channels

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.technologies.capslassestment.core.data.pojo.channel.Channel
import com.technologies.capslassestment.feature.dashboard.channel.GetChannels
import javax.inject.Inject


class ChannelsDataSourceFactory  @Inject constructor(
    private val getChannels: GetChannels,
    private val loading: MutableLiveData<Boolean>,
    private val empty: MutableLiveData<Boolean>
) : DataSource.Factory<Int, Channel>() {

    val source = MutableLiveData<ChannelsDataSource>()

    override fun create(): DataSource<Int, Channel> {
        val source = ChannelsDataSource(getChannels, loading, empty)
        this.source.postValue(source)
        return source
    }
}
