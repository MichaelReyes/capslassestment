package com.technologies.capslassestment.core.extension

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.afollestad.materialdialogs.MaterialDialog

val Context.networkInfo: NetworkInfo? get() =
    (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo

fun Context.showConfirmationMaterialDialog(title: String, message: String, positiveLabel: String, negativeLabel: String, positiveCallback: () -> Unit, negativeCallback: (() -> Unit)? = null){
    MaterialDialog(this).show {
        title(text = title)
        message(text = message)
        positiveButton(text = positiveLabel){
            positiveCallback()
            it.dismiss()
        }
        negativeButton(text = negativeLabel){
            negativeCallback?.invoke()
            it.dismiss()
        }
    }
}