package com.technologies.capslassestment.core.network.api.channel

import com.technologies.capslassestment.BuildConfig
import com.technologies.capslassestment.core.data.pojo.channel.Channels
import com.technologies.capslassestment.core.data.pojo.generic.BaseApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ChannelsApi {

    @GET("channel/${BuildConfig.APP_ID}")
    fun channels(
        @Query("page_no") pageNumber: Int,
        @Query("page_size") pageSize: Int
    ): Call<BaseApiResponse<Channels>?>
}