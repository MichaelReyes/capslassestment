package com.technologies.capslassestment.core.data.pojo.auth

data class Account(
    val uid: Int,
    val userAccount: String
)