package com.technologies.capslassestment.core.di.module

import com.technologies.capslassestment.MainActivity
import com.technologies.capslassestment.core.di.scope.ActivityScope
import com.technologies.capslassestment.feature.dashboard.DashboardActivity
import com.technologies.capslassestment.feature.dashboard.live.LiveActivity
import com.technologies.capslassestment.feature.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindDashboardActivity(): DashboardActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindLiveActivity(): LiveActivity

}