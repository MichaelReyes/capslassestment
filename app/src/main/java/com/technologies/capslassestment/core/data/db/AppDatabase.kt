package com.technologies.capslassestment.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

/*

@Database(entities = [],
        version = AppDatabase.VERSION, exportSchema = false)
*/
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "capslapp.db"
        const val VERSION = 1
    }
}