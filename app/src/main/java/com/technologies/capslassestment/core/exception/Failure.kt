package com.technologies.capslassestment.core.exception

import okhttp3.ResponseBody
import retrofit2.HttpException

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureFailure] class.
 */
sealed class Failure {
    object NetworkConnection : Failure()
    object ServerError : Failure()
    class ApiException(exception: HttpException) : Failure()
    class ApiError(errorBody: ResponseBody?) : Failure()

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailure: Failure()
}
