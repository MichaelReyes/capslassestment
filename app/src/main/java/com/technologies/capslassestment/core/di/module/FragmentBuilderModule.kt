package com.technologies.capslassestment.core.di.module


import com.technologies.capslassestment.core.di.scope.FragmentScope
import com.technologies.capslassestment.feature.dashboard.channel.ChannelsFragment
import com.technologies.capslassestment.feature.dashboard.identification.IdentificationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindChannelsFragment(): ChannelsFragment

    @FlowPreview
    @ExperimentalCoroutinesApi
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindIdentificationFragment(): IdentificationFragment

}