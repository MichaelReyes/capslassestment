package com.technologies.capslassestment.core.di.module

import android.content.Context
import com.technologies.capslassestment.BuildConfig
import com.technologies.capslassestment.core.base.App
import com.technologies.capslassestment.core.di.scope.AppScope
import com.technologies.capslassestment.core.utils.rtc.AgoraEventHandler
import com.technologies.capslassestment.core.utils.rtc.EngineConfig
import com.technologies.capslassestment.core.utils.rtc.StatsManager
import dagger.Module
import dagger.Provides
import io.agora.rtc.Constants
import io.agora.rtc.RtcEngine
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideContext(app: App): Context {
        return app
    }

    @Provides
    @AppScope
    fun provideRtcEngine(context: Context, eventHandler: AgoraEventHandler): RtcEngine {
        val rtcEngine = RtcEngine.create(context, BuildConfig.APP_ID, eventHandler)
        rtcEngine.apply {
            setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING)
            enableVideo()
        }
        return rtcEngine
    }

    @Provides
    @AppScope
    fun provideRtcEventHandler(): AgoraEventHandler {
        return AgoraEventHandler()
    }

    @Provides
    @AppScope
    fun provideStatsManager(): StatsManager {
        return StatsManager()
    }

    @Provides
    @AppScope
    fun provideEngineConfig(): EngineConfig {
        return EngineConfig()
    }

}