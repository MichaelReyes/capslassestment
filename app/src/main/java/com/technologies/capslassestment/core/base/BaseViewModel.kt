package com.technologies.capslassestment.core.base



import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.technologies.capslassestment.core.exception.Failure
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel: ViewModel() {

    protected val disposable = CompositeDisposable()

    private val _failure = MutableLiveData<Failure>()
    val failure: LiveData<Failure> = _failure

    protected fun handleFailure(failure: Failure) {
        setLoading(false)
        _failure.value = failure
    }

    private val _loading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _loading

    protected fun setLoading(value: Boolean){ _loading.value = value }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}