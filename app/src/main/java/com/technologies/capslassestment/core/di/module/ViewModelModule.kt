package com.technologies.capslassestment.core.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.technologies.capslassestment.feature.dashboard.channel.ChannelsViewModel
import com.technologies.capslassestment.feature.dashboard.identification.IdentificationViewModel
import com.technologies.capslassestment.feature.dashboard.live.LiveViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ChannelsViewModel::class)
    abstract fun bindsChannelsViewModel(viewModel: ChannelsViewModel): ViewModel

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(IdentificationViewModel::class)
    abstract fun bindsIdentificationViewModel(viewModel: IdentificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LiveViewModel::class)
    abstract fun bindsLiveViewModel(viewModel: LiveViewModel): ViewModel
}
