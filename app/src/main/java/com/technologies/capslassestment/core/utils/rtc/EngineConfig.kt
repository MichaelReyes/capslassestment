package com.technologies.capslassestment.core.utils.rtc

import com.technologies.capslassestment.core.data.pojo.auth.Account

data class EngineConfig(
    var account: Account? = null,
    var showVideoStats: Boolean = false
)