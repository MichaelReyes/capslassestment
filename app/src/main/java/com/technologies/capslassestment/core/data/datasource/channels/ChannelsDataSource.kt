package com.technologies.capslassestment.core.data.datasource.channels

import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import com.technologies.capslassestment.core.data.pojo.channel.Channel
import com.technologies.capslassestment.core.exception.Failure
import com.technologies.capslassestment.feature.dashboard.channel.GetChannels
import io.reactivex.disposables.CompositeDisposable

class ChannelsDataSource(private val getChannels: GetChannels,
                         private val loading: MutableLiveData<Boolean>,
                         private val empty: MutableLiveData<Boolean>) : ItemKeyedDataSource<Int, Channel>() {

    private var pageNumber = 0
    private var size = 10

    var params: LoadParams<Int>? = null
    var callback: LoadCallback<Channel>? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Channel>
    ) {
        loading.postValue(true)
        getChannels(GetChannels.Params(pageNumber, size)){
            it.fold(::handleFailure) { channels -> onChannelsFetched(channels, callback) }
        }
    }

    private fun onChannelsFetched(channels: List<Channel>, callback: LoadInitialCallback<Channel>) {
        loading.postValue(false)
        pageNumber+=size
        empty.postValue(channels.isEmpty())
        callback.onResult(channels)
    }

    private fun handleFailure(failure: Failure) {
        empty.postValue(true)
        loading.postValue(false)
    }


    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Channel>) {
        this.params = params
        this.callback = callback
        loading.postValue(true)
        getChannels(GetChannels.Params(params.key, size)){
            it.fold(::handleFailure) { channels -> onMoreChannelsFetched(channels, callback) }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Channel>) {}

    override fun getKey(item: Channel): Int = pageNumber

    private fun onMoreChannelsFetched(users: List<Channel>, callback: LoadCallback<Channel>) {
        loading.postValue(false)
        pageNumber+=size
        callback.onResult(users)
    }

    private fun onPaginationError(throwable: Throwable) {
        throwable.printStackTrace()
    }

}
