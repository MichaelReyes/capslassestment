package com.technologies.capslassestment.core.data.pojo.channel

data class Channel(
    val channel_name: String,
    val user_count: Int
)