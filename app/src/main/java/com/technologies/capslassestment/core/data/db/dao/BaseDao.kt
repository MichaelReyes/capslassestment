package com.technologies.capslassestment.core.data.db.dao

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insert(vararg t: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insert(t: List<T>)

    @Update
    suspend fun update(vararg t: T)

    @Delete
    suspend fun delete(vararg t: T)
}